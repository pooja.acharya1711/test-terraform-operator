resource "aws_s3_bucket" "test-s3-bucket" {
  count = 1
  bucket = "pooja-testkiam"
  acl    = "private"
  tags = {
    Name        = "pooja-testkiam"
    Environment = var.env
  }
}
