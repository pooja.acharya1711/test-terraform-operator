provider "aws" {
  region = "us-east-1"
}

module "s3-test" {
  source = "./mod"
  bucket-name = var.bucket-name
  env = var.env
}
